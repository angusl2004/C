/*
 * This includes pre-compiled code for standard
 * input, output, and error from the C standard
 * library.
 *
 * Brackets are used because it is including a
 * standard library header file that is
 * pre-designated by the compiler or IDE.
 *
 * You use double quotes if you want to include
 * and search for a programmer-defined header
 * file.
 */
#include <stdio.h>

/**
 * All possible combinations of integers:
 *
 * short, signed short, unsigned short
 * int, signed, unsigned
 * long, signed long, unsigned long
 * long long, signed long long, unsigned long long
 *
 * The default is to be signed. int can be appended to the
 * end as an option.
 *
 * char, signed char, and unsigned char is CHAR_BIT bits.
 * CHAR_BIT is at least 8 bits long, or 1 byte.
 *
 * float is 32-bit single precision.
 * double is 64-bit  double precision.
 * long double is 80-bit extended precision.
 *
 * This is the main function.
 */
int main(void)
{
	/*
	 * The length of char is -127 to 127 on this
	 * particular 64-bit system.
	 *
	 * You must use %hhi(signed) or %hhu(unsigned) in
	 * order to have a numerical output in printf().
	 *
	 * Use %c if you want a character.
	 *
	 * It is often possible to use char as a method
	 * of storing small integers.
	 */
	char ch = 2;
	/*
	 * The range of unsigned int is from 0 to
	 * 4,294,967,295 because this being written
	 * on a 64-bit system.
	 *
	 * int is capable of at least having a range of
	 * -32767 to 32767.
	 */
	unsigned int un = 3000000000;
	/*
	 * The range of short is at least -32,767 to 32,767.
	 */
	short end = 200;
	/*
	 * The range of long is at least -2,147,483,647 to
	 * 2,147,483,647. In this case, it is the same as
	 * long long since this is code written on a
	 * 64-bit system.
	 */
	long big = 65537;
	/*
	 * The range of long long is at least
	 * -9,223,372,036,854,775,807 to
	 * 9,223,372,036,854,775,807.
	 */
	long long veryBig = 12345678908642;

	puts("A byte is 8 bits");
	/*
	 * sizeof() returns an unsigned long int, therefore
	 * the placeholder must be %lu(l = long; u = unsigned).
	 */
	printf("char is %lu bytes in length on this system\n", sizeof(ch));
	printf("unsigned is %lu bytes in length on this system\n", sizeof(un));
	printf("short is %lu bytes in length on this system\n", sizeof(end));
	printf("long is %lu bytes in length on this system\n", sizeof(big));
	printf("long long is %lu bytes in length on this system\n", sizeof(veryBig));
	/*
	 * Puts() must have an argument, otherwise the compiler
	 * will complain and will not allow compilation is happen.
	 * "" is passed in, in this case.
	 */
	puts("");
	/*
	 * If you use %d for an unsigned int, you get a signed
	 * representation back.
	 */
	printf("un = %u and not %d\n", un, un);
	/*
	 * %hd is a short designation.
	 *
	 * %d works because C will expand short to an int when
	 * it is passed in as an argument to a function.
	 */
	printf("end = %hd and %d\n", end, end);
	/*
	 * 65537 in binary is 00000000000000010000000000000001.
	 * By using %hd, you tell printf() to look at only the last
	 * 16 bits of the 32-digit number. This results in %hd
	 * outputting 1.
	 */
	printf("big = %ld and not %hd\n", big, big);
	/*
	 * On a 32-bit system, this would print out different values.
	 * The same sort of thing is suppose to happen here, as with
	 * the long int. But in this case, %ld is suppose to only look
	 * at the last 32 bits of the 64-bit number.
	 */
	printf("veryBig = %lld and not %ld\n", veryBig, veryBig);
	/*
	 * This returns an exit value indicating
	 * a successful program run.
	 */
	return(0);
}
