/**
 * This will include precompiled code from the
 * C standard library, and it allows you to have
 * standard input, output, and error.
 *
 * When data is typed at the keyboard, it does not
 * go straight to the program. Instead, it goes to
 * something called a buffer. When you use scanf(),
 * you may accidently read from what is left in the
 * buffer as opposed to what you actually want read in.
 * To fix this, you may have to flush the buffer.
 *
 * You can flush the buffer using fflush(stdin), but
 * this may be bad practice since your code may not
 * work properly if you take your code to a different
 * computer with a different compiler.
 *
 * In general, most unix implementations would have
 * stdin and stidout line-buffered when associated
 * with a terminal, otherwised it is fully buffered.
 * stderr is always unbuffered.
 */
#include <stdio.h>
/*
 * A note: This program currently contains some sort
 * of error. The input is prompted before the printf
 * statements are run.
 *
 * The problem was fixed by disabling buffering.
 *
 * This is the main function.
 */
int main(void)
{
	//This disables the buffering for the output stream.
	setvbuf(stdout, NULL, _IONBF, 0);

	float weight; //Declare weight.
	float value;  //Declare value.

	/*
	 * You can use puts() when you don't have to be
	 * concerned with placeholders. This function will
	 * automatically append a newline character at the
	 * end of its output.
	 */
	puts("Are you worth your weight in rhodium?");
	puts("Let's check it out.");
	//I want scanf() to be read in at the end of the sentence.
	printf("Please enter your weight in pounds: ");

	/*
	 * Here we are using scanf(). You specify the data type
	 * the computer will be looking for by having a format
	 * string. Then you have to point to a location in memory.
	 * This can be done with either a pointer or with
	 * an &(this means the address-of).
	 *
	 * If you place a space behind the placeholder,
	 * this will tell the compiler to ignore all of
	 * the whitespace preceding it.
	 *
	 * Do not use & when you are reading in a string. This
	 * is because the variable that you will be using
	 * to store the string is an array, and the array
	 * name themselves will already point to the base
	 * address.
	 *
	 * scanf() will read and store input from stdin until
	 * it is filled. The rest of the input buffer will be
	 * left alone, and will need to be cleared or used.
	 */
	scanf(" %f", &weight);

	//The floats used in this equation are called constants.
	value = 770.0 * weight * 14.5833;

	printf("Your weight is %.0f pounds.\n", weight);
	/*
	 * The .2 in the placeholder tells the compiler to
	 * chop the floating point at 2 decimal places.
	 */
	printf("Your weight in rhodium is worth $%.2f.\n", value);
	puts("You are easily worth that! If rhodium prices drop.");
	puts("eat more to maintain your value.");

	return(0); //Returns an exit value of 0.
}
