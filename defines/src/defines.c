// defines.c -- uses defined constants from limit.h and float.
#include <stdio.h>
#include <limits.h> // integer limits
#include <float.h> // floating-point limits
/*
 * This is the main function.
 */
int main(void) {
	   printf("THE INTEGRAL TYPE LIMITS\n");
	   printf("\n");
	   printf("The number of bits in a byte %d\n", CHAR_BIT);
	   printf("\n");
	   printf("The minimum value of CHAR = %d\n", CHAR_MIN);
	   printf("The maximum value of CHAR = %d\n", CHAR_MAX);
	   printf("The minimum value of SIGNED CHAR = %d\n", SCHAR_MIN);
	   printf("The maximum value of SIGNED CHAR = %d\n", SCHAR_MAX);
	   printf("The maximum value of UNSIGNED CHAR = %d\n", UCHAR_MAX);
	   printf("\n");
	   printf("The value of MB_LEN_MAX = %d\n", MB_LEN_MAX);
	   printf("\n");
	   printf("The minimum value of SHORT = %d\n", SHRT_MIN);
	   printf("The maximum value of SHORT = %d\n", SHRT_MAX);
	   printf("The maximum value of UNSIGNED SHORT = %d\n", USHRT_MAX);
	   printf("\n");
	   printf("The minimum value of INT = %d\n", INT_MIN);
	   printf("The maximum value of INT = %d\n", INT_MAX);
	   printf("The maximum value of UNSIGNED INT = %u\n", UINT_MAX);
	   printf("\n");
	   printf("The minimum value of LONG = %ld\n", LONG_MIN);
	   printf("The maximum value of LONG = %ld\n", LONG_MAX);
	   printf("The maximum value of UNSIGNED LONG = %lu\n", ULONG_MAX);
	   printf("\n");
	   printf("The minimum value of LONG LONG = %lld\n", LLONG_MIN);
	   printf("The maximum value of LONG LONG = %lld\n", LLONG_MAX);
	   printf("The maximum value of UNSIGNED LONG LONG = %llu\n", ULLONG_MAX);
	   printf("\n");
	   printf("THE FLOATING POINT LIMITS\n");
	   printf("\n");
	   printf("The value of FLT_RADIX = %d\n", FLT_RADIX);
	   printf("\n");
	   printf("# of digits round-able before changing radix point for float = %d\n", FLT_DIG);
	   printf("# of digits round-able before changing radix point for double = %d\n", DBL_DIG);
	   printf("# of digits round-able before changing radix point for long double = %d\n", LDBL_DIG);
	   printf("\n");
	   printf("The mantissa length of float = %d\n", FLT_MANT_DIG);
	   printf("The mantissa length of double = %d\n", DBL_MANT_DIG);
	   printf("The mantissa length of long double = %d\n", LDBL_MANT_DIG);
	   printf("\n");
	   printf("The minimal exponent of float = %d\n", FLT_MIN_EXP);
	   printf("The minimal exponent of double = %d\n", DBL_MIN_EXP);
	   printf("The minimal exponent of long double = %d\n", LDBL_MIN_EXP);
	   printf("\n");
	   printf("The minimal exponent in BASE 10 of float = %d\n", FLT_MIN_10_EXP);
	   printf("The minimal exponent in BASE 10 of double = %d\n", DBL_MIN_10_EXP);
	   printf("The minimal exponent in BASE 10 of long double = %d\n", LDBL_MIN_10_EXP);
	   printf("\n");
	   printf("The maximum exponent of float = %d\n", FLT_MAX_EXP);
	   printf("The maximum exponent of double = %d\n", DBL_MAX_EXP);
	   printf("The maximum exponent of long double = %d\n", LDBL_MAX_EXP);
	   printf("\n");
	   printf("The maximum exponent in BASE 10 of float = %d\n", FLT_MAX_10_EXP);
	   printf("The maximum exponent in BASE 10 of double = %d\n", DBL_MAX_10_EXP);
	   printf("The maximum exponent in BASE 10 of long double = %d\n", LDBL_MAX_10_EXP);
	   printf("\n");
	   /*
	    * This prints out the largest representable value in float.
	    */
	   printf("The maximum represented value of float = %f\n", FLT_MAX);
	   /*
	    * This may go off the screen...
	    */
	   printf("The maximum represented value of double = %f\n", DBL_MAX);
	   /*
	    * This might REALLY go off the screen...
	    */
	   printf("The maximum represented value of long double = %Lf\n", LDBL_MAX);
	   printf("\n");
	   /*
	    * This prints out the smallest representable value is float.
	    */
	   printf("The minimal represented value of float = %f\n", FLT_MIN);
	   /*
	    * This prints out the smallest representable value is double.
	    */
	   printf("The minimal represented value of double = %f\n", DBL_MIN);
	   /*
	    * This prints out the smallest representable value is long double.
	    */
	   printf("The minimal represented value of long double = %Lf\n", LDBL_MIN);
	   printf("\n");

	   /*
	    * Returns an exit value indicating a successful program run.
	    */
	   return 0;
}
