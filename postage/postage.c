// postage.c -- first-class postage rates

/*
 * This includes pre-compiled code for standard
 * input, output, and error streams from the C
 * standard library.
 */
#include <stdio.h>
/*
 * This is the main function.
 */
int main(void)
{
	/*
	 * The maximum specified ounce quantity.
	 */
	const int MAX_OZ = 16;
	/*
	 * The rate of the first ounce.
	 */
	const int FIRST_OZ = 37;
	/*
	 * The rate for each additional ounce.
	 */
	const int NEXT_OZ = 23;
	/*
	 * The number of ounces.
	 */
	int ounces;
	/*
	 * The total cost of a specific ounce
	 * quantity.
	 */
	int cost;
	/*
	 * A header for the quantity of ounces and the
	 * cost of each specific quantity.
	 */
	printf(" ounces cost\n");
	/*
	 * This is a demonstration of the comma operator
	 * in a for-loop.
	 *
	 * The comma operator can be applied either in the
	 * initialization or the update expression.
	 *
	 * In this example, ounces is initialized to 1 and
	 * cost is the rate of the first ounce. As long as
	 * ounces is less than or equal to MAX_OZ, increment
	 * ounce and add NEXT_OZ to cost.
	 */
	for (ounces = 1, cost = FIRST_OZ; ounces <= MAX_OZ; ounces++, cost += NEXT_OZ)
	{
		printf("%5d $%4.2f\n", ounces, cost / 100.0);
	}
	/*
	 * Returns an exit value indicating a successful
	 * program run.
	 */
	return 0;
}
