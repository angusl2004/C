/**
 * This will include precompiled code from the
 * C standard library, and it allows you to have
 * standard input, output, and error.
 *
 * When data is typed at the keyboard, it does not
 * go straight to the program. Instead, it goes to
 * something called a buffer. output is sent to the
 * buffer when either the buffer is full, when a
 * newline character is encountered, or when there
 * is an impending input.
 *
 * When you use scanf() and you are using an
 * implementation that doesn't force a flush when
 * scanf() is executed, you may accidently read from
 * what is left in the buffer as opposed to what you
 * actually want read in. To fix this, you may have
 * to flush the buffer.
 *
 * You can flush the buffer using fflush(stdin), but
 * this may be bad practice since your code may not
 * work properly if you take your code to a different
 * computer with a different compiler.
 *
 * In general, most unix implementations would have
 * stdin and stidout line-buffered when associated
 * with a terminal, otherwised it is fully buffered.
 * stderr is always unbuffered.
 */
#include <stdio.h>

/*
 * This is the main function.
 */
int main(void) // The main method header should always take in a argument of void
{
	printf("Goodbye, cruel world!");
	/*
	 * This returns an exit value indicating a
	 * successful program run.
	 */
	return(0);
}
