/*
 * This will include precompiled code for the standard input,
 * output, and error stream. This will be from the C standard
 * library.
 */
#include<stdio.h>

/*
 * This is the main function.
 */
int main(void)
{
	/*
	 * This disables buffering for the standard output stream.
	 *
	 * NULL is a null pointer used to disable buffering.
	 */
	setbuf(stdout, NULL);
	/*
	 * This will store an integer that can be used to point
	 * to a character on a character table.
	 */
	char ch;

	/*
	 * Only strings are used, puts() is used for simplicity.
	 */
	puts("Please enter a character.");
	/*
	 * scan in a character from the user, store it in the
	 * address of ch.
	 */
	scanf("%c", &ch);
	/*
	 * When you use %c, printf() will return a character
	 * representation of the char integer. When you use %d,
	 * printf() returns the char integer itself.
	 */
	printf("The code for %c is %d.\n", ch, ch);
	/*
	 * %hhi is also a legal placeholder for returning a
	 * signed char integer and will not return a compiler
	 * error.
	 */
	printf("The code for %c is %hhi.\n", ch, ch);

	/*
	 * Returns an exit value indicating a successful run.
	 */
	return(0);
}
