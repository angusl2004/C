#include <stdio.h>


int number = 2; //A variable declared outside a method.

int main()
{
	int num = 1; //A variable declared inside a method.

	printf("I am a simple ");
	printf("computer. \n"); // This will print out at the end of the previous printf

	// Ex. of substitution
	printf("My favourite number is %d because it is first.\n", num);
	 //Substitution with an outside variable
	printf("My second favourite number is %d because it is second.\n", number);

	/**
	 * The following statements are to test out legal substitutions
	 * in a printf statement.
	 */
	printf("%d %d\n", num, number); // legal statement
	//printf("%d %d\n", num); //error, needs a second argument
	//printf("%d %d %d\n", num, number); //error, needs a third argument

	// Conclusion: A variable can only be paired to one %d
}
