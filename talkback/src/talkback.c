/*
 * talkback.c -- nosy, informative program
 */

/**
 * This will include precompiled code from the
 * C standard library, and it allows you to have
 * standard input, output, and error.
 *
 * When data is typed at the keyboard, it does not
 * go straight to the program. Instead, it goes to
 * something called a buffer. output is sent to the
 * buffer when either the buffer is full, when a
 * newline character is encountered, or when there
 * is an impending input.
 *
 * When you use scanf() and you are using an
 * implementation that doesn't force a flush when
 * scanf() is executed, you may accidently read from
 * what is left in the buffer as opposed to what you
 * actually want read in. To fix this, you may have
 * to flush the buffer.
 *
 * You can flush the buffer using fflush(stdin), but
 * this may be bad practice since your code may not
 * work properly if you take your code to a different
 * computer with a different compiler.
 *
 * In general, most unix implementations would have
 * stdin and stidout line-buffered when associated
 * with a terminal, otherwised it is fully buffered.
 * stderr is always unbuffered.
 */
#include <stdio.h>
/*
 * for strlen() prototype
 *
 * This header file contains several function prototypes for
 * string-related function. Use this if you have a pre-ANSI
 * compiler.
 */
#include <string.h>
/*
 * This is a symbolic constant for human density in pounds
 * per cubic feet.
 *
 * This is where you declare you constants like you would
 * in Java. These constants are also called manifest constants.
 * Notice that constants in C are also upper case like in
 * Java. You do not need a semi-colon because this is a
 * substitution mechanism and not a statement.
 *
 * When you use DENSITY in your program, the compiler would
 * substitute DENSITY with 62.4. This is called a compile-time
 * substitution.
 */
#define DENSITY 62.4

/*
 * This is the main function.
 */
int main(void)
{
	/*
	 * This disables buffering for the standard output
	 * stream. This is done for simplicity, as oppose to
	 * having to flush the buffer.
	 */
	setbuf(stdout, NULL);
	/*
	 * This will contain a value passed in by the user for
	 * their weight.
	 */
	float weight;
	/*
	 * This will contain a value indicating the total
	 * volume of the person.
	 */
	float volume;
	/*
	 * This will contain a value indicating the size of
	 * the array.
	 */
	int size;
	/*
	 * This will contain a value indicating the length
	 * of a string.
	 */
	int letters;
	/*
	 * This is an array of 40 characters that will contain
	 * the name of the person.
	 *
	 * When you store a string in an array, remember that a
	 * string terminates with the \0 character. This
	 * character is called the null character, and it marks
	 * the end of the string. This is done automatically when
	 * passing in a string from scanf() or when initializing
	 * a string.
	 *
	 * This is also why "x" and 'x' is not the same; "x" would
	 * contain x and \0, whilst 'x' would only contain x.
	 *
	 * Make sure the array is at least one or more elements
	 * greater than the number of characters to be stored to
	 * account for the null character.
	 */
	char name[40];

	printf("Hi! What's your first name?\n");
	/*
	 * Take note that name does not need a ampersand
	 * prefix. This is because an array name will
	 * automatically point to the base address in
	 * memory.
	 */
	scanf("%s", name);
	printf("%s, what's your weight in pounds?\n", name);
	scanf("%f", &weight);

	/*
	 * Each element has a byte to store data, since char on
	 * this system is exactly a byte. Therefore, the size of
	 * the array would be the sum of all the space created
	 * for the 40 characters.
	 */
	size = sizeof name;
	/*
	 * strlen() is a function that will find the length of
	 * a string.
	 *
	 * A string is any char array with a string constant passed
	 * in. A string can be passed in by using double quotes as
	 * opposed to single quotes for char.
	 */
	letters = strlen(name);
	/*
	 * This is an operation based entirely in floating point with
	 * only float variables.
	 */
	volume = weight / DENSITY;

	printf("Well, %s, your volume is %2.2f cubic feet.\n",
			name, volume);
	printf("Also, your first name has %d letters,\n",
			letters);
	printf("and we have %d bytes to store it in.\n", size);

	/*
	 * This returns a value indicating a successful program
	 * run.
	 */
	return 0;
}
