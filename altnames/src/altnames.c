/*
 * This includes pre-compiled code for standard
 * input, output, and error streams from the C
 * standard library.
 */
#include <stdio.h>
/*
 * This includes pre-compiled code for the
 * standard library. I am using the EXIT_SUCCESS
 * constant that is available from this header
 * file.
 */
#include <stdlib.h>
/*
 * This includes pre-compiled code for portable
 * types from the C standard library.
 *
 * Portable types derive its name from the
 * portability created by strictly defining
 * the width of integers for any system rather
 * than having to tailor your code each time
 * to the word-length of a system.
 */
#include <inttypes.h>

/*
 * This is the main function. All is normal.
 */
int main(void)
{
	/*
	 * This creates an alias to an integer type
	 * that is 16 bits long. It is then initialized
	 * to a hard-coded random number.
	 */
	int16_t me16 = 4593;
	/*
     * This assumes that int16_t is a short. This
     * may not always be true, since int16_t can be
     * either a short or an int.
     *
     * This is what you might do if you do
     * not use inttypes.h and its macros.
     */
	printf("First, assume int16_t is short: ");
	/*
	 * The reason why this is legal is because C will
	 * combine consecutive quoted strings into a single
	 * quoted string.
	 *
	 * Example:
	 *
	 * printf("me16 = %" "hd" "\n", me16);
	 *
	 * becomes...
	 *
	 * printf("me16 = %hd\n", me16);
	 */
	printf("me16 = %" "hd" "\n", me16);
	/*
	 * This is why inttypes.h is used rather than
	 * just stdint.h.
	 *
	 * Not only does inttypes.h provide typedefs for
	 * integers, it also has macros that you can use
	 * for scanf() and printf().
	 *
	 * Using macros makes the specifiers target more
	 * specifically, as opposed to assuming an alias
	 * is of a certain type by using regular integer
	 * specifiers.
	 */
	printf("Next, let's not make any assumptions.\n");
	printf("Instead, use a \"macro\" from inttypes.h: ");
	/*
	 * The trick that C does with combining consecutive
	 * quoted strings is why PRId16 works.
	 *
	 * PRId16 is replaced with "hd" (Note: with the quotes),
	 * therefore becoming exactly like the second printf()
	 * statement of this program.
	 */
	printf("me16 = %"PRId16"\n", me16);
	/*
	 * Returns an exit value indicating a
	 * successful program run.
	 */
	return EXIT_SUCCESS;
}
