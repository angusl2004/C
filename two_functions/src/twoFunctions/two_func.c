#include<stdio.h>
/*
 * This is a function prototype. This will inform the
 * compiler that this function will be used.
 */
void butler(void);

/**
 * This is the main method.
 */
int main (void)
{
	printf("I will summon the butler .\n");
	butler(); // This is the method call for the butler
	printf("Yes. Bring me some tea and writeable CD-ROMS\n");
}

/**
 * This is the butler method. This will respond to its
 * master.
 */
void butler(void)
{
	printf("You rang, sir?\n");
}
