/*
 * This includes pre-compiled code for standard
 * input, output, and error streams from the C
 * standard library.
 */
#include <stdio.h>
/*
 * This includes pre-compiled code for the
 * standard library. I am using the EXIT_SUCCESS
 * constant that is available from this header
 * file.
 */
#include <stdlib.h>
/*
 * If you initialize a float with a mantissa greater
 * than 6 digit, the compiler will truncate everything
 * after 6 digits.
 *
 * If you initialize an int with a float, it will
 * truncate the decimal part. It will not round the
 * decimal place.
 *
 * This is the main function. It is really boring.
 */
int main(void) {
	float aboat = 32000.0;
	double abet = 2.14e9;
	//long double dip = 5.32e-5;

	printf("%f can be written %e\n", aboat, aboat);
	printf("%f can be written %e\n", abet, abet);
	//printf("%f can be written %e\n", dip, dip);
	/*
	 * Returns an exit value indicating a
	 * successful program run.
	 */
	return EXIT_SUCCESS;
}
